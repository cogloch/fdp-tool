#include "shaders.h"


GLuint shaders::CompileShader(const char* source, GLenum type)
{
    GLuint shader = gl::CreateShader(type);
    gl::ShaderSource(shader, 1, &source, nullptr);
    gl::CompileShader(shader);

    GLint success;
    gl::GetShaderiv(shader, gl::COMPILE_STATUS, &success);
    if (!success)
    {
        GLchar infoLog[512];
        gl::GetShaderInfoLog(shader, 512, nullptr, infoLog);
        throw std::string("Shader compilation failed: " + std::string(infoLog));
    }

    return shader;
}

GLuint shaders::CreateProgram(const std::string& vertShaderSource, const std::string& fragShaderSource)
{
    GLuint vertShader = CompileShader(vertShaderSource.c_str(), gl::VERTEX_SHADER);
    GLuint fragShader = CompileShader(fragShaderSource.c_str(), gl::FRAGMENT_SHADER);

    GLuint program = gl::CreateProgram();
    gl::AttachShader(program, vertShader);
    gl::AttachShader(program, fragShader);
    gl::LinkProgram(program);

    GLint success;
    gl::GetProgramiv(program, gl::LINK_STATUS, &success);
    if (!success)
    {
        GLchar infoLog[512];
        gl::GetProgramInfoLog(program, 512, nullptr, infoLog);
        throw std::string("Shader program linking failed: " + std::string(infoLog));
    }

    gl::DetachShader(program, vertShader);
    gl::DetachShader(program, fragShader);
    gl::DeleteShader(vertShader);
    gl::DeleteShader(fragShader);

    return program;
}