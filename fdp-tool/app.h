#pragma once
#include "wing_mgr.h"
#include "work_mgr.h"
#include "renderer.h"
#include "camera.h"
#include "timer.h"
#include "plot.h"
#include <glfw/glfw3.h>


class UIState;

class App
{
public:
    App();

    void RunMainLoop();

private:
    AirfoilMgr m_airfoilMgr;
    WingMgr m_wingMgr;
    WorkMgr m_workMgr;
    GLFWwindow* m_pWindow;
    Renderer* m_pRenderer;

    Camera m_camera;
	Timer m_timer;

private:
    void InitWindow();
    void InitUI();
	void ApplyUIStateChanges(UIState& ui);
};