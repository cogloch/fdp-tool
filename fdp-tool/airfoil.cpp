#include "xfoil_pipe.h"
#include "airfoil.h"
#include "common.h"
#include <algorithm>
#include <iostream>
#include <sstream>
#include <chrono>
#include <atomic>
#include <array>
#include <windows.h>
#include <Shlwapi.h>
#pragma comment(lib, "shlwapi.lib")

#include <sqlite_modern_cpp\sqlite_modern_cpp.h>

#include <xlsxwriter.h>

#undef min
#undef max


// TEMP 
// Keep around for debugging; will do everything directly 
struct Sample
{
	float x;
	float yc, yt;
	float dycdx; // dyc/dx
	float theta;
	glm::vec2 upper;
    glm::vec2 lower;

	void Compute4(float m, float p, float xx)
	{
		// Camber and gradient
		float xsq = x * x,
			  psq = p * p,
			  oneMinP = 1 - p,
			  oneMinPSq = oneMinP * oneMinP;

		if (x < p)  // Front section
		{
			yc = (m / psq) * (2 * p * x - xsq);
			dycdx = (2 * m / psq) * (p - x);
		}
		else        // Back section
		{
			yc = (m / oneMinPSq) * (1 - (2 * p) * (1 - x) - xsq);
			dycdx = (2 * m / oneMinPSq * (p - x));
		}

		theta = atanf(dycdx);
		
        // Thickness distribution
		static const float a0 = 0.2969f,
						   a1 = -0.126f,
						   a2 = -0.3516f,
						   a3 = 0.2843f,
						   a4 = -0.1015f; // Open trailing edge
						// a4 = -0.1036f; // Closed trailing edge

		float k0 = a0 * sqrtf(x),
			  k1 = a1 * x,
			  k2 = a2 * x * x,
			  k3 = a3 * x * x * x,
			  k4 = a4 * x * x * x * x;

		yt = 5 * xx * (k0 + k1 + k2 + k3 + k4);

		// Upper surface
		upper.x = x - yt * sinf(theta);
		upper.y = yc + yt * cosf(theta);

		// Lower surface
		lower.x = x + yt * sinf(theta);
		lower.y = yc - yt * cosf(theta);
	} 

    void Compute5Lead(float r, float k1)
    {
        yc = (k1 / 6.f) * (powf(x, 3) - 3.f * r * powf(x, 2) + powf(r, 2) * (3.f - r) * x);
        dycdx = (k1 / 6.f) * (3.f * powf(x, 2), - 6.f * r * x + powf(r, 2) * (3.f - r));
    }

    void Compute5Trail(float r, float k1)
    {
        yc = (k1 / 6.f) * powf(r, 3) * (1.f - x);
        dycdx = -(k1 / 6.f) * powf(r, 3.f);
    }

    void Compute5(float p, float xx)
    {
        int m = p * 20.f; // Original 2nd digit 

        // NASA Technical Memorandum 4741, pg 8 
        // https://ntrs.nasa.gov/archive/nasa/casi.ntrs.nasa.gov/19970008124.pdf
        const float r[] = {
            .0580f,  // 5%
            .1260f,  // 10%
            .2025f,  // 15%
            .2900f,  // 20%
            .3910f   // 25%
        };

        const float k1[] = {
            361.40f,  // 5%
            51.640f,  // 10%
            15.957f,  // 15%
            6.6430f,  // 20%
            3.2300f   // 25%
        };

        if (x <= p)
            Compute5Lead(r[m], k1[m]);
        else
            Compute5Trail(r[m], k1[m]);

        theta = atanf(dycdx);

        // Thickness distribution
        const float a0 = 0.2969f,
                    a1 = -0.126f,
                    a2 = -0.3516f,
                    a3 = 0.2843f,
                    a4 = -0.1015f; // Open trailing edge
                 // a4 = -0.1036f; // Closed trailing edge

        const float t0 = a0 * sqrtf(x),
                    t1 = a1 * x,
                    t2 = a2 * powf(x, 2),
                    t3 = a3 * powf(x, 3),
                    t4 = a4 * powf(x, 4);

        yt = 5 * xx * (t0 + t1 + t2 + t3 + t4);

        // Upper surface
        upper.x = x - yt * sinf(theta);
        upper.y = yc + yt * cosf(theta);

        // Lower surface
        lower.x = x + yt * sinf(theta);
        lower.y = yc - yt * cosf(theta);
    }
};

std::array<std::vector<glm::vec2>, 2> GenerateNACA4(const std::string & code, int numSamples)
{
    // mpxx
    int codeNum = std::stoi(code);
    int maxCamber = codeNum / 1000;         // m
    int maxCamberPos = codeNum / 100 % 10;  // p
    int thickness = codeNum % 100;          // xx

    enum
    {
        UPPER_SURFACE,
        LOWER_SURFACE
    };

    std::array<std::vector<glm::vec2>, 2> result;
    result[UPPER_SURFACE].reserve(numSamples);
    result[LOWER_SURFACE].reserve(numSamples);

    // Percentages
    float m = static_cast<float>(maxCamber) / 100.0f,
          p = static_cast<float>(maxCamberPos) / 10.0f,
          xx = static_cast<float>(thickness) / 100.0f;

    auto samples = new Sample[numSamples];

    samples[0].x = 0.0f;
    samples[0].Compute4(m, p, xx);
    result[UPPER_SURFACE].push_back(samples[0].upper);
    result[LOWER_SURFACE].push_back(samples[0].lower);

    auto increment = 1.0f / static_cast<float>(numSamples);
    for (int i = 1; i < numSamples; ++i)
    {
        samples[i].x = samples[i - 1].x + increment;
        samples[i].Compute4(m, p, xx);

        result[UPPER_SURFACE].push_back(samples[i].upper);
        result[LOWER_SURFACE].push_back(samples[i].lower);
    }

    delete[] samples;
    return result;
}

std::array<std::vector<glm::vec2>, 2> GenerateNACA5(const std::string & code, int numSamples)
{
    // 2p0xx
    int codeNum = std::stoi(code);
    int maxCamberPos = codeNum / 1000 % 10; // p
    int thickness = codeNum % 100;          // xx

    enum
    {
        UPPER_SURFACE,
        LOWER_SURFACE
    };

    std::array<std::vector<glm::vec2>, 2> result;
    result[UPPER_SURFACE].reserve(numSamples);
    result[LOWER_SURFACE].reserve(numSamples);

    float p = static_cast<float>(maxCamberPos) / 20.0f,
          xx = static_cast<float>(thickness) / 100.0f;

    auto samples = new Sample[numSamples];

    samples[0].x = 0.0f;
    samples[0].Compute5(p, xx);
    result[UPPER_SURFACE].push_back(samples[0].upper);
    result[LOWER_SURFACE].push_back(samples[0].lower);

    auto increment = 1.0f / static_cast<float>(numSamples);
    for (int i = 1; i < numSamples; ++i)
    {
        samples[i].x = samples[i - 1].x + increment;
        samples[i].Compute5(p, xx);

        result[UPPER_SURFACE].push_back(samples[i].upper);
        result[LOWER_SURFACE].push_back(samples[i].lower);
    }

    delete[] samples;
    return result;
}

std::vector<glm::vec2> MergeUpperLowerVec(std::array<std::vector<glm::vec2>, 2> & surfaces)
{
    enum
    {
        UPPER_SURFACE,
        LOWER_SURFACE
    };

    std::vector<glm::vec2> merged;
    merged.reserve(surfaces[UPPER_SURFACE].size() + surfaces[LOWER_SURFACE].size());

    for (auto rit = surfaces[UPPER_SURFACE].rbegin(), rend = surfaces[UPPER_SURFACE].rend(); rit != rend; ++rit)
    {
        merged.push_back(*rit);
    }
    for (auto it = surfaces[LOWER_SURFACE].begin(), end = surfaces[LOWER_SURFACE].end(); it != end; ++it)
    {
        merged.push_back(*it);
    }

    return merged;
}

// TODO fill container 
void ProcessXfoilData(AirfoilDataQueue & queue, AirfoilContainer & container)
{
    sqlite::database db("airfoils.db");
    db << "drop table if exists airfoils";

    db <<
        "create table if not exists airfoils ("
        "   _id integer primary key autoincrement not null,"
        "   code text,"
        "   alpha text,"
        "   cl text,"
        "   cd text"
        ");";


    int timeoutCounter = 10;

    for (;;)
    {
        std::string code;
        queue.Pop(code);
        if (code == "iwishiwereagoodperson") break;
        std::string filename = "temp/" + code;

        int delay = 10;
        HANDLE hFile = nullptr;
        bool ok = true;
        do
        {
            hFile = CreateFileA(filename.c_str(), GENERIC_READ, FILE_SHARE_READ, nullptr, OPEN_EXISTING, 0, nullptr);

            if (GetLastError() == ERROR_SHARING_VIOLATION)
            {
                Sleep(delay);
                if (delay < 2560) // 10 * 2 ^ 8, 2.5 sec 
                    delay *= 2;
            }
            else if (GetFileSize(hFile, nullptr) < 1500) // Jesus christ 
            {
                Sleep(delay);
                if (delay < 2560)
                    delay *= 2;
                else
                {
                    std::cout << "Messy airfoil: " << filename << '\n';
                    ok = false;
                    break;
                }
                CloseHandle(hFile);
                hFile = nullptr;
            }
            else
            {
                //__debugbreak();
                break;
            }
        } while (!hFile || hFile == INVALID_HANDLE_VALUE);

        if (!ok)
            continue;

        auto size = GetFileSize(hFile, nullptr);

        CHAR buffer[4096];
        DWORD bytes = 0;
        ReadFile(hFile, buffer, 4096, &bytes, nullptr);
        CloseHandle(hFile);

        // Remove "header" 
        size_t start = 0;
        for (; start < bytes; ++start)
        {
            if (buffer[start] == '-')
            {
                start += 65;
                break;
            }
        }

        std::string data;
        std::move(buffer + start, buffer + bytes, std::back_inserter(data));

        AirfoilMeta meta;
        meta.name = code;

        // TODO variable sample size, points should be ~2mm apart for the foam cutter 
        AirfoilGeom geom;
        if(code.size() == 4)
            geom.coords = MergeUpperLowerVec(GenerateNACA4(code, 30)); 
        else
            geom.coords = MergeUpperLowerVec(GenerateNACA5(code, 30)); 

        AirfoilData aero;
        aero.key = container.data.size();
        float dummy;
        std::istringstream stream(data);
        std::string alphaStore, clStore, cdStore;
        for (int i = 0; i < 26 * 7; ++i)
        {
            if (stream.eof())
                break;

            AirfoilCoeff coeff;
            stream >> coeff.alpha >> coeff.cl >> coeff.cd >> dummy >> dummy >> dummy >> dummy;
            aero.coeffs.push_back(coeff);

            alphaStore += std::to_string(coeff.alpha) + " ";
            clStore += std::to_string(coeff.cl) + " ";
            cdStore += std::to_string(coeff.cd) + " ";
        }

        CalculateAirfoilPerformance(&aero);
        container.Add(meta, aero, geom);

        try
        {
            db << "insert into airfoils (code,alpha,cl,cd) values (?,?,?,?);"            
               << code
               << alphaStore
               << clStore
               << cdStore;
        }
        catch (std::exception & err)
        {
            std::cout << err.what() << '\n';
            __debugbreak();
        }

        if (!DeleteFileA(filename.c_str()))
        {
            auto err = GetLastError();
            __debugbreak();
        }
    }
}

AirfoilMgr::AirfoilMgr()
{
    if (!Load("airfoils.db"))
        RebuildDb("airfoils.db");

    Rank();

    HANDLE dummyFile = CreateFileA("airfoil ranking dump.xlsx", GENERIC_READ, 0, nullptr, OPEN_EXISTING, 0, nullptr);
    if (dummyFile == INVALID_HANDLE_VALUE)
        DumpExcel();
    else
        CloseHandle(dummyFile);
}

void AirfoilMgr::AddSingle(const std::string & code, float reynolds)
{
    // TODO refactor

    AirfoilDataQueue queue;
    AirfoilContainer tempContainer;
    GetXfoilNACA(code, reynolds, -5.f, 20.f, 1.f, queue);
    std::thread processDataThread(ProcessXfoilData, std::ref(queue), std::ref(tempContainer));
    queue.Push("iwishiwereagoodperson");
    processDataThread.join();
    MergeContainer(std::move(tempContainer));
}

void BatchRebuildDb(AirfoilDataQueue & queue, float reynolds)
{
    // TODO fix getting stuck on a "pacc" 
    std::vector<std::string> codes;

    std::string code = "0000";
    for (int hc = 15; hc <= 20; ++hc)
    {
        code[2] = hc / 10 + 48;
        code[3] = hc % 10 + 48;

        for (int a = '0'; a <= '9'; ++a)
        {
            code[0] = a;

            for (char b = '0'; b <= '9'; ++b)
            {
                code[1] = b;
                codes.push_back(code);
            }
        }
    }

    code = "20000";
    for (int hc = 15; hc <= 20; ++hc)
    {
        code[3] = hc / 10 + 48;
        code[4] = hc % 10 + 48;

        for (char b = '1'; b <= '5'; ++b)
        {
            code[1] = b;
            codes.push_back(code);
        }
    }

    BatchXfoilNACA(codes, reynolds, -5.f, 20.f, 1.f, queue);
}

void DumbRebuildDb(AirfoilDataQueue & queue, float reynolds = 595593.f)
{
    const char upper4 = '9'; // TODO temp, define ranges later 
    const char upper5 = '5'; // TODO temp, define ranges later 

    std::string code = "0000";
    for (int hc = 15; hc <= 20; ++hc)
    {
        code[2] = hc / 10 + 48;
        code[3] = hc % 10 + 48;

        for (int a = '0'; a <= upper4; ++a)
        {
            code[0] = a;

            for (char b = '0'; b <= upper4; ++b)
            {
                code[1] = b;
                GetXfoilNACA(code, reynolds, -5.f, 20.f, 1.f, queue);
            }
        }
    }

    code = "20000";
    for (int hc = 15; hc <= 20; ++hc)
    {
        code[3] = hc / 10 + 48;
        code[4] = hc % 10 + 48;

        for (char b = '1'; b <= upper5; ++b)
        {
            code[1] = b;
            GetXfoilNACA(code, reynolds, -5.f, 20.f, 1.f, queue);
        }
    }
}

double GetAirfoilDragCoeff(const AirfoilData* airfoil, double alpha)
{
    // TODO lerp
    for (auto& coef : airfoil->coeffs)
        if (coef.alpha >= alpha)
            return coef.cd;

    // TODO something about this case
    return std::numeric_limits<double>::max();
}

void CalculateAirfoilPerformance(AirfoilData* airfoil)
{
    airfoil->clMax = std::numeric_limits<double>::lowest();

    double closestToZero = std::numeric_limits<double>::max();
    double closestToHalf = std::numeric_limits<double>::max();
    u32 alphaAtCl0Idx = 0; // Index of closest alpha to the real alpha at which Cl = 0
    u32 alpha0Idx = 0;     // Index of alpha = 0
    u32 halfClIdx = 0;     // Index of closest Cl to 0.5
    for (u32 coefIdx = 0; coefIdx < airfoil->coeffs.size(); ++coefIdx)
    {
        if (airfoil->coeffs[coefIdx].cl > airfoil->clMax)
        {
			airfoil->clMax = airfoil->coeffs[coefIdx].cl;
			airfoil->clMaxAlphaIdx = coefIdx;
        }

        if (fabs(airfoil->coeffs[coefIdx].cl) < fabs(closestToZero))
        {
            closestToZero = airfoil->coeffs[coefIdx].cl;
			airfoil->alpha0 = airfoil->coeffs[coefIdx].alpha;
            alphaAtCl0Idx = coefIdx;
        }

        if (airfoil->coeffs[coefIdx].alpha == 0)
        {
            alpha0Idx = coefIdx;
        }
    }

    halfClIdx = std::distance(airfoil->coeffs.begin(), std::min_element(airfoil->coeffs.begin(), airfoil->coeffs.end(), [=](const AirfoilCoeff & a, const AirfoilCoeff & b) -> bool {
        return abs(a.cl - 0.5) < abs(b.cl - 0.5);
    }));

    // Lift curve slope
    airfoil->clSlope = (airfoil->coeffs[alphaAtCl0Idx + 1].cl - airfoil->coeffs[alphaAtCl0Idx].cl) / (airfoil->coeffs[alphaAtCl0Idx + 1].alpha - airfoil->coeffs[alphaAtCl0Idx].alpha);

    //airfoil->takeoffPerf = airfoil->coeffs[alphaAtCl0Idx].cd - 0.025 * airfoil->coeffs[alphaAtCl0Idx].cl;
    airfoil->takeoffPerf = airfoil->coeffs[alpha0Idx].cd - 0.025 * airfoil->coeffs[alpha0Idx].cl;
    airfoil->cd0 = airfoil->coeffs[alphaAtCl0Idx].cd;
    airfoil->loiterCd = airfoil->coeffs[halfClIdx].cd;
}

void AirfoilMgr::Clear()
{
    m_airfoils.Reset();
}

void AirfoilMgr::Rank()
{
    auto numAirfoils = m_airfoils.meta.size();
    auto & data = m_airfoils.data;

    // Takeoff
    std::sort(data.begin(), data.end(), [](const AirfoilData & a, const AirfoilData & b) -> bool {
        return a.takeoffPerf < b.takeoffPerf;
    });
    for (u32 i = 0; i < numAirfoils; ++i)
        data[i].scorecard.takeoffRank = i;

    // Dash
    std::sort(data.begin(), data.end(), [](const AirfoilData & a, const AirfoilData & b) -> bool {
        return a.cd0 < b.cd0;
    });
    for (u32 i = 0; i < numAirfoils; ++i)
        data[i].scorecard.dashRank = i;

    // Loiter
    std::sort(data.begin(), data.end(), [](const AirfoilData & a, const AirfoilData & b) -> bool {
        return a.loiterCd < b.loiterCd;
    });
    for (u32 i = 0; i < numAirfoils; ++i)
        data[i].scorecard.loiterRank = i;

    // Land
    std::sort(data.begin(), data.end(), [](const AirfoilData & a, const AirfoilData & b) -> bool {
        return a.clMax > b.clMax;
    });
    for (u32 i = 0; i < numAirfoils; ++i)
        data[i].scorecard.landRank = i;

    // Total
    for (auto & airfoil : data)
        airfoil.scorecard.CalcTotal();

    // Reset and sort by total 
    std::sort(data.begin(), data.end(), [](const AirfoilData & a, const AirfoilData & b) -> bool {
        return a.key < b.key;
    });

    for (u32 i = 0; i < numAirfoils; ++i)
    {
        m_airfoils.meta[i].rank = m_airfoils.data[i].scorecard.totalRank;
        m_airfoils.geom[i].rank = m_airfoils.data[i].scorecard.totalRank;
    }

    std::sort(data.begin(), data.end(), [](const AirfoilData & a, const AirfoilData & b) -> bool {
        return a.scorecard.totalRank < b.scorecard.totalRank;
    });

    std::sort(m_airfoils.geom.begin(), m_airfoils.geom.end(), [](const AirfoilGeom & a, const AirfoilGeom & b) -> bool {
        return a.rank < b.rank;
    });

    std::sort(m_airfoils.meta.begin(), m_airfoils.meta.end(), [](const AirfoilMeta & a, const AirfoilMeta & b) -> bool {
        return a.rank < b.rank;
    });
}

void AirfoilMgr::DumpExcel()
{
    lxw_workbook * workbook = workbook_new("airfoil ranking dump.xlsx");
    lxw_worksheet * worksheet = workbook_add_worksheet(workbook, NULL);

    worksheet_write_string(worksheet, 0, 0, "Code", NULL);
    worksheet_write_string(worksheet, 0, 1, "Takeoff", NULL);
    worksheet_write_string(worksheet, 0, 2, "Dash", NULL);
    worksheet_write_string(worksheet, 0, 3, "Loiter", NULL);
    worksheet_write_string(worksheet, 0, 4, "Land", NULL);
    worksheet_write_string(worksheet, 0, 5, "Total", NULL);

    for (u32 i = 0; i < m_airfoils.meta.size(); ++i)
    {
        worksheet_write_string(worksheet, i + 1, 0, ("NACA " + m_airfoils.meta[i].name).c_str(), NULL);
        std::ostringstream formatted;
        formatted << m_airfoils.data[i].scorecard.takeoffRank << " (" << m_airfoils.data[i].takeoffPerf << ")";
        worksheet_write_string(worksheet, i + 1, 1, formatted.str().c_str(), NULL);

        formatted = std::ostringstream();
        formatted << m_airfoils.data[i].scorecard.dashRank << " (" << m_airfoils.data[i].cd0 << ")";
        worksheet_write_string(worksheet, i + 1, 2, formatted.str().c_str(), NULL);

        formatted = std::ostringstream();
        formatted << m_airfoils.data[i].scorecard.loiterRank << " (" << m_airfoils.data[i].loiterCd << ")";
        worksheet_write_string(worksheet, i + 1, 3, formatted.str().c_str(), NULL);

        formatted = std::ostringstream();
        formatted << m_airfoils.data[i].scorecard.landRank << " (" << m_airfoils.data[i].clMax << ")";
        worksheet_write_string(worksheet, i + 1, 4, formatted.str().c_str(), NULL);

        formatted = std::ostringstream();
        formatted << m_airfoils.data[i].scorecard.totalRank << " (#" << i + 1 << ")";
        worksheet_write_string(worksheet, i + 1, 5, formatted.str().c_str(), NULL);
    }

    workbook_close(workbook);
}

void AirfoilMgr::RebuildDb(const std::string & dbPath)
{
    // TODO clean up temp/
    AirfoilDataQueue queue;
    AirfoilContainer tempContainer;
    std::thread processDataThread(ProcessXfoilData, std::ref(queue), std::ref(tempContainer));

    //BatchRebuildDb(queue);
    DumbRebuildDb(queue);
    queue.Push("iwishiwereagoodperson");
    processDataThread.join();

    MergeContainer(std::move(tempContainer));
}

bool AirfoilMgr::Load(const std::string & dbPath)
{
    AirfoilContainer tempContainer;
    
    try
    {
        sqlite::database db(dbPath);
        std::string dummy;
        db << "select name from sqlite_master where type='table' and name='airfoils';"
           >> dummy;

        db << "select code, alpha, cl, cd from airfoils;"
            >> [&](std::string code, std::string alpha, std::string cl, std::string cd) {
            std::stringstream alphas(alpha);
            std::stringstream cls(cl);
            std::stringstream cds(cd);

            AirfoilMeta meta;
            meta.name = code;

            AirfoilData data;
            data.key = tempContainer.data.size();
            while (!alphas.eof())
            {
                AirfoilCoeff coeff;
                alphas >> coeff.alpha;
                cls >> coeff.cl;
                cds >> coeff.cd;
                data.coeffs.push_back(std::move(coeff));
            }
            CalculateAirfoilPerformance(&data);

            AirfoilGeom geom;
            if (code.size() == 4)
                geom.coords = MergeUpperLowerVec(GenerateNACA4(code, 30));
            else
                geom.coords = MergeUpperLowerVec(GenerateNACA5(code, 30));

            tempContainer.Add(meta, data, geom);
        };
    }
    catch (std::exception & err)
    {
        std::cout << err.what();
        return false;
    }

    MergeContainer(std::move(tempContainer));
    return true;
}

void AirfoilMgr::MergeContainer(AirfoilContainer && container)
{
    for (u32 i = 0; i < container.meta.size(); ++i)
    {
        bool bExists = false;
        for (u32 j = 0; j < m_airfoils.meta.size(); ++j)
        {
            if (container.meta[i].name == m_airfoils.meta[j].name)
            {
                bExists = true;
                break;
            }
        }

        if (!bExists)
        {
            m_airfoils.Add(container.meta[i], container.data[i], container.geom[i]);
        }
    }
}

size_t AirfoilMgr::GetNumAirfoils() const
{
	return m_airfoils.data.size();
}

AirfoilData& AirfoilMgr::GetAirfoilRef(u32 idx)
{
	return m_airfoils.data[idx];
}

AirfoilGeom & AirfoilMgr::GetAirfoilGeom(u32 idx)
{
	return m_airfoils.geom[idx];
}

const AirfoilGeom & AirfoilMgr::GetAirfoilConstGeom(u32 idx) const
{
	return m_airfoils.geom[idx];
}

const AirfoilMeta& AirfoilMgr::GetAirfoilMeta(u32 idx) const
{
	return m_airfoils.meta[idx];
}

const AirfoilData& AirfoilMgr::GetAirfoilConstRef(u32 idx) const
{
	return m_airfoils.data[idx];
}

AirfoilContainer AirfoilMgr::GetCopy() const
{
	return m_airfoils;
}

void AirfoilContainer::Add(AirfoilMeta& newMeta, AirfoilData& newData, AirfoilGeom& newGeom)
{
	meta.push_back(std::move(newMeta));
	data.push_back(std::move(newData));
	geom.push_back(std::move(newGeom));
}

void AirfoilContainer::CopyAirfoil(AirfoilMeta newMeta, AirfoilData newData, AirfoilGeom newGeom)
{
	meta.push_back(newMeta);
	data.push_back(newData);
	geom.push_back(newGeom);
}

// TODO reuse data, meta objects
void AirfoilContainer::Reset()
{
	meta.clear();
	data.clear();
	geom.clear();
}

void AirfoilData::Scorecard::CalcTotal()
{
    totalRank = takeoffRank + dashRank + loiterRank + landRank;
}
