#include "socket.h"


namespace sock
{
    WSADATA wsaData;
    SOCKET conn;
    std::string hostname;

    void Init()
    {
        auto err = WSAStartup(MAKEWORD(2, 2), &wsaData);
        if (err)
            throw std::string("WSAStatup failed: " + std::to_string(err));

        // AF_INET for IPv4
        // AF_INET6 for IPv6
        //socket = WSASocketW(AF_INET, SOCK_STREAM, IPPROTO_TCP, NULL, 0, 0/*WSA_FLAG_OVERLAPPED*/);
        conn = ::socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
        if (INVALID_SOCKET == conn)
            throw std::string("Socket creation failed: " + std::to_string(WSAGetLastError()));
    }

    void Cleanup()
    {
        closesocket(conn);
        WSACleanup();
    }

    void Connect(const std::string& url)
    {
        // TODO extract hostname from generic url 
        hostname = url;

        auto host = gethostbyname(hostname.c_str());
        if (!host)
            throw std::string("Host information retrieval for " + hostname + " failed: " + std::to_string(WSAGetLastError()));

        SOCKADDR_IN sockAddr;
        sockAddr.sin_port = htons(80);
        sockAddr.sin_family = AF_INET;
        sockAddr.sin_addr.s_addr = *reinterpret_cast<unsigned long*>(host->h_addr);

        auto err = connect(conn, reinterpret_cast<SOCKADDR*>(&sockAddr), sizeof(sockAddr));
        if (err)
            throw std::string("Connection to " + hostname + " failed: " + std::to_string(WSAGetLastError()));
    }

    std::string HTTP_GET(const std::string& path)
    {
        std::string getHTTP = "GET " + path + " HTTP/1.1\nHost: " + hostname + "\nConnection: close\n\n";
        auto err = send(conn, getHTTP.c_str(), getHTTP.length(), 0);
        if (SOCKET_ERROR == err)
            throw "Failed to send HTTP request: " + std::to_string(WSAGetLastError());

        char buffer[512];
        std::string result;
        for (;;)
        {
            auto recvSz = recv(conn, buffer, 512, 0);
            if (SOCKET_ERROR == recvSz)
                throw "Failed to receive HTTP response: " + std::to_string(WSAGetLastError());
            if (!recvSz)
                break;

            result.append(buffer, buffer + recvSz);
        }

        // TODO this is beyond retarded  
        closesocket(conn);
        Init();
        Connect(hostname);

        return result;
    }
}