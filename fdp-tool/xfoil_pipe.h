#pragma once
#include <string>
#include <vector>

#include <thread>
#include <mutex>
#include <queue>
#include <condition_variable>


struct AirfoilDataQueue
{
    void Pop(std::string &);
    void Push(std::string &&);
    
private:
    std::mutex mutex;
    std::queue<std::string> files;
    std::condition_variable condVar;
};


// Either 4 digit or 5 digit NACA code 
void GetXfoilNACA(const std::string & code, float Re, float minAlpha, float maxAlpha, float incAlpha, AirfoilDataQueue &);
void BatchXfoilNACA(const std::vector<std::string> & codes, float Re, float minAlpha, float maxAlpha, float incAlpha, AirfoilDataQueue &);