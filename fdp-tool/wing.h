#pragma once
#include "mesh.h"
#include "airfoil.h"
#include <vector>
#include <thread>
#include <rapidjson/document.h>
#include <rapidjson/prettywriter.h>

#undef min

using AirfoilId = int;
const AirfoilId INVALID_AIRFOIL_ID = -1;

struct Material
{
	double density; // kg/m^3
	double youngMod; // N/mm^2
	double shearMod; // N/mm^2
	double tensileStrength; // N/mm^2
	double compressiveStress; // N/mm^2, at 10% deformation
	double shearStrength; // N/mm^2
};

extern const Material foam;

struct Ring
{
	Ring();

	std::vector<glm::vec3> points;
	std::vector<glm::vec3> localPts; // Used for geometry calculations 

	bool Analyze(); // Returns false if the ring is useless 
	void Reset(u32 numPoints);

	void AddPoint(float x, float y, float z);
	void AddPoint(const glm::vec3&);

	double GetArea() const;
	double GetMinThickness() const;

	double GetIxx() const;
	double GetIyy() const;
	double GetIxy() const;

	static const double minThicknessThreshold;

	void AlignToXY(float z);

private:
	double Ixx, Iyy, Ixy;
	double area;
	double minThickness;
};

struct LoadingTest
{
	float loading = 0.f;
	double deflection = -1.0;
	u32 numCuts = 0;
	bool bAlign = false;

	bool operator!=(const LoadingTest& other) const;
};

class WingMgr;

class Wing
{
public:
	// TODO temp
	static WingMgr* s_pWingMgr;
	AirfoilMeta rootMeta, tipMeta;
	AirfoilData rootData, tipData;
	AirfoilGeom rootGeom, tipGeom;

	explicit Wing(u32 rootAirfoil, u32 tipAirfoil, u32 id);
	Wing& operator=(const Wing&) = default;
	Wing(const Wing&) = default;
	bool operator==(const Wing&) const;
	bool operator!=(const Wing&) const;
	bool CurrentBetter(const Wing&) const;
	bool BestBetter(const Wing&) const;    // Fuck me 

	void Serialize(rapidjson::PrettyWriter<rapidjson::StringBuffer>&) const;
	void Deserialize(rapidjson::Value&);
	void ExportToGCode(std::string& gcodePath) const;

	void ApplyTipLoading(LoadingTest&, Mesh* pOutMesh = nullptr);
	LoadingTest loadingTest;

	void CalculatePerformance(AirfoilData& root, AirfoilData& tip, AirfoilGeom& rootGeom, AirfoilGeom& tipGeom, AirfoilMeta& tipMeta, AirfoilMeta& rootMeta);
	void GenerateGeometry();
	void GenerateMesh(Mesh* pOut);
	void SetToBest();
	const Wing* GetBestConfig() const;

	// TODO temp
	// Jesus fucking christ 
	bool IsValid() const;
	u32 GetId() const;
	u32 GetRootAirfoilId() const;
	u32 GetTipAirfoilId() const;
	void SetRootAirfoilId(u32);
	void SetTipAirfoilId(u32);
	void SetMaxClCd(float);
	void SetSweep(float);
	void SetTaperRatio(float);
	void SetTwist(float);
	void SetDihedral(float);
	void SetRootChord(float);
	double GetLiftCoeff(float alpha) const;
	double GetDragCoeff(float alpha) const;
	float GetAlpha0() const;
	float GetAlphaStall() const;
	float GetSweep() const;
	float GetTaperRatio() const;
	float GetTwist() const;
	float GetDihedral() const;
	float GetRootChord() const;
	float GetClmax() const;
	float GetClslope() const;
	float GetMaxClCd() const;
	float GetHalfVol() const;
	float NeedReduceDepth() const;
	float NeedReduceHeight() const;
	const Ring& GetTip() const;
	const Ring& GetRoot() const;

private:
	u32 id;
	u32 bestConfigId;

	Ring m_root, m_tip;
	std::vector<Ring> m_slices; // Spanwise cuts between root and tip 

								// Configurable
	float sweep = 0.f;          // In deg from leading edge 
	float taperRatio = 1.f;
	float twist = 0.f;          // In deg 
	float rootChord = 20.f;     // Max: 30cm
	float dihedral = 0.f;       // In deg
	AirfoilId rootAirfoil = INVALID_AIRFOIL_ID;
	AirfoilId tipAirfoil = INVALID_AIRFOIL_ID;

	// Performance 
	double clmax = 0, alphaStall = 0;
	double clSlope = 0;
	double alpha0 = 0;
	double maxClCd = std::numeric_limits<double>::lowest();
	double halfVolume = 0.0; // Left and right wing halves are identical 
	double aspectRatio;

	// Cutting limits 
	static const double depthLimit, heightLimit;
	static const float halfSpan;
	bool bNeedReduceDepth = false;
	bool bNeedReduceHeight = false;
};