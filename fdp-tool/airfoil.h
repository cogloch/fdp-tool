#pragma once
#include <vector>
#include <string>
#include "common.h"
#include <glm/glm.hpp>


struct AirfoilCoeff
{
	double alpha;   // Angle of incidence
	double cl;      // Lift coefficient at <alpha>
	double cd;      // Drag coefficient at <alpha>
};

struct AirfoilMeta
{
	std::string name;

    u32 rank; // dumb, dumb, dumb
};

struct AirfoilGeom
{
	//   2 -------- 1
	//   |          |
	//   3 -------- 4
	std::vector<glm::vec2> coords;

    u32 rank; // dumb, dumb, dumb
};

struct AirfoilData
{
    u32 key;

	std::vector<AirfoilCoeff> coeffs;
	u32 clMaxAlphaIdx; // Index in the coeffs vector 
	double clSlope;
	double alpha0;     // Zero lift incidence 

    double takeoffPerf; // Cd - 0.025 * Cl at alpha = 0 
    double cd0;         // Zero lift Cd
    double loiterCd;    // Cd at Cl = 0.5
	double clMax;

    struct Scorecard
    {
        u32 takeoffRank;  // Min Cd0 - 0.025 * Cl at alpha = 0
        u32 dashRank;     // Min Cd0
        u32 loiterRank;   // Min Cd at Cl = 0.5 
        u32 landRank;     // Max Clmax
        
        u32 totalRank;    // Min is the best 
        void CalcTotal();
    } scorecard;
};

double GetAirfoilDragCoeff(const AirfoilData*, double alpha);
void CalculateAirfoilPerformance(AirfoilData*);



// The manager has 3 containers with the same number of elements, one for Airfoil(s), another for AirfoilMeta(s), and for AirfoilGeometry
// so that for the i'th Airfoil object, there is a corresponding i'th AirfoilMeta object 
struct AirfoilContainer
{
	std::vector<AirfoilMeta> meta;
	std::vector<AirfoilData> data;
	std::vector<AirfoilGeom> geom;

	void Add(AirfoilMeta&, AirfoilData&, AirfoilGeom&); // Will destroy the passed arg
	void CopyAirfoil(AirfoilMeta, AirfoilData, AirfoilGeom);
	void Reset();
};

class AirfoilMgr
{
public:
    AirfoilMgr();

    // Just the 4-digit or 5-digit code, without "NACA " or w/e 
    void AddSingle(const std::string & code, float reynolds);
    void RebuildDb(const std::string & dbPath);
    bool Load(const std::string & dbPath);
    void Clear();

    void Rank();
    void DumpExcel();

    void MergeContainer(AirfoilContainer &&);

	size_t GetNumAirfoils() const;
	AirfoilData& GetAirfoilRef(u32 idx);
	AirfoilGeom& GetAirfoilGeom(u32 idx);
	const AirfoilGeom& GetAirfoilConstGeom(u32 idx) const;
	const AirfoilMeta& GetAirfoilMeta(u32 idx) const;
	const AirfoilData& GetAirfoilConstRef(u32 idx) const;
	AirfoilContainer GetCopy() const;

private:
	AirfoilContainer m_airfoils;
};