#include "plot.h"
#include <cmath>


void Plot::LinePlot(std::vector<glm::vec2> points)
{
    ImGui::Begin(parentWindow.c_str(), 0, ImGuiWindowFlags_NoTitleBar);

    auto pDrawList = ImGui::GetWindowDrawList();
    auto canvasPos = ImGui::GetCursorScreenPos();
    auto  canvasSz = ImGui::GetContentRegionAvail();

    if (canvasSz.x < minWidth) canvasSz.x = minWidth;
    if (canvasSz.y < minHeight) canvasSz.y = minHeight;

    DrawBg(pDrawList, canvasPos, canvasSz);

    // Scale data to match plot size 
    float xMin = std::numeric_limits<float>::max(), xMax = std::numeric_limits<float>::lowest();
    float yMin = std::numeric_limits<float>::max(), yMax = std::numeric_limits<float>::lowest();

    for (const auto& pt : points)
    {
        if (pt.x > xMax) xMax = pt.x;
        if (pt.x < xMin) xMin = pt.x;
        if (pt.y > yMax) yMax = pt.y;
        if (pt.y < yMin) yMin = pt.y;
    }

    const float xRange = xMax - xMin,
        yRange = yMax - yMin;

    if (bAllowStretching)
    {
        for (auto& pt : points)
        {
            pt.x = fabs((pt.x - xMin) / xRange) * canvasSz.x + canvasPos.x;
            pt.y = canvasSz.y - fabs((pt.y - yMin) / yRange) * canvasSz.y + canvasPos.y;
        }
    }
    else
    {
        for (auto& pt : points)
        {
            pt.x = fabs((pt.x - xMin) / xRange) * canvasSz.x + canvasPos.x;
            pt.y = canvasPos.y + pt.y * (canvasSz.x / xRange) + canvasSz.y / 2.f;
        }
    }

    pDrawList->PushClipRect(canvasPos, ImVec2(canvasPos.x + canvasSz.x, canvasPos.y + canvasSz.y));
    for (int i = 0; i < points.size() - 1; ++i)
    {
        const ImVec2 from = { points[i].x, points[i].y };
        const ImVec2 to = { points[i + 1].x, points[i + 1].y };

        pDrawList->AddLine(from, to, dataColor, dataThickness);
    }
    pDrawList->PopClipRect();

    ImGui::End();
}

void Plot::DrawBg(ImDrawList* pDrawList, const ImVec2 canvasPos, const ImVec2 canvasSz) const
{
    const auto leftEdge = canvasPos.x,
        rightEdge = canvasPos.x + canvasSz.x;
    const auto topEdge = canvasPos.y,
        botEdge = canvasPos.y + canvasSz.y;

    pDrawList->AddRectFilled(canvasPos, { rightEdge, botEdge }, bgColor);  // Background
    pDrawList->AddRect(canvasPos, { rightEdge, botEdge }, borderColor);    // Border 

                                                                           // Grid 
    pDrawList->AddLine({ canvasPos.x, canvasPos.y + canvasSz.y / 2 }, { canvasPos.x + canvasSz.x, canvasPos.y + canvasSz.y / 2 }, axesColor, 2.0f); // Ox
    pDrawList->AddLine({ canvasPos.x + canvasSz.x / 2, canvasPos.y }, { canvasPos.x + canvasSz.x / 2, canvasPos.y + canvasSz.y }, axesColor, 2.0f); // Oy

    const auto xIncrement = canvasSz.x / numGridLines;
    const auto yIncrement = canvasSz.y / numGridLines;

    for (int i = 0; i < numGridLines; ++i)
    {
        pDrawList->AddLine({ leftEdge, topEdge + yIncrement * i }, { rightEdge, topEdge + yIncrement * i }, gridlinesColor, .1f); // Vertical lines 
        pDrawList->AddLine({ leftEdge + xIncrement * i, topEdge }, { leftEdge + xIncrement * i, botEdge }, gridlinesColor, .1f);  // Horizontal lines 
    }
}
