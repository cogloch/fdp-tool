#pragma once
#include <gl/glcore.hpp>
#include <string>


namespace shaders
{
    GLuint CompileShader(const char* source, GLenum type);
    GLuint CreateProgram(const std::string& vertShaderSource, const std::string& fragShaderSource);
}